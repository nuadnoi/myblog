package blog

type BlogData struct {
	Id         string `bson:"_id"`
	Name       string `bson:"name"`
	Detail     string `bson:"detail"`
	Created_at string `bson:"created_at"`
	Updated_at string `bson:"updated_at"`
}
