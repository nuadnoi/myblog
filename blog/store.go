package blog

type Store interface {
	GetOne(id string) (*BlogData, error)
	GetAll() ([]BlogData, error)
	Save(input *BlogData) error
	Edit(id string, input map[string]interface{}) error
	Delete(id string) error
}
