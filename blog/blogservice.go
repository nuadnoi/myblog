package blog

import (
	"github.com/Pallinder/go-randomdata"
	"github.com/plimble/unik"
	"time"
)

type BlogService struct {
	store Store
}

func NewService(s Store) *BlogService {
	return &BlogService{s}
}

func (b *BlogService) Get(id string) *BlogData {
	var result *BlogData
	result, _ = b.store.GetOne(id)

	return result
}

func (b *BlogService) GetAll() []BlogData {
	var result []BlogData
	result, _ = b.store.GetAll()

	return result
}

func (b *BlogService) Add() (*BlogData, error) {
	now := time.Now().String()
	data := &BlogData{
		Id:         unik.NewSnowflake(0).Generate(),
		Name:       randomdata.SillyName(),
		Detail:     randomdata.Paragraph(),
		Created_at: now,
		Updated_at: now,
	}

	if err := b.store.Save(data); err != nil {
		return data, err
	}

	return data, nil
}

func (b *BlogService) Edit(id string) (*BlogData, error) {
	data := map[string]interface{}{}
	data["name"] = randomdata.SillyName()

	err := b.store.Edit(id, data)

	if err != nil {
		return &BlogData{}, err
	}

	result := b.Get(id)
	return result, err
}

func (b *BlogService) Delete(id string) error {
	err := b.store.Delete(id)

	return err
}
