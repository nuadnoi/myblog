package mongo

import (
	"github.com/Pallinder/go-randomdata"
	"github.com/plimble/unik"
	"github.com/stretchr/testify/assert"
	//"gopkg.in/mgo.v2/bson"
	"myblog/blog"
	"testing"
	"time"
)

func TestGetOne(t *testing.T) {
	s := GetSession()
	now := time.Now().String()

	resultSave, errSave := s.Save(&blog.BlogData{
		Id:         unik.NewSnowflake(0).Generate(),
		Name:       randomdata.SillyName(),
		Detail:     randomdata.Paragraph(),
		Created_at: now,
		Updated_at: now,
	})
	assert.NoError(t, errSave)

	resultGet, errGet := s.GetOne(resultSave.Id)
	assert.NoError(t, errGet)
	assert.Equal(t, resultSave, resultGet)

	data := map[string]interface{}{}
	data["name"] = "NEWNAME55555555"

	errEdit := s.Edit(resultGet.Id, data)
	resultGet, errGet = s.GetOne(resultSave.Id)
	assert.NoError(t, errEdit)
	assert.Equal(t, "New valuettt", resultGet.Name)
}
