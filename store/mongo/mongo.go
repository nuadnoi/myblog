package mongo

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"myblog/blog"
)

type SessionStore struct {
	*mgo.Session
	Collection *mgo.Collection
}

func GetSession(db string, col string) (*SessionStore, error) {
	MURL := "mongodb://nuadnoi:123456abc@ds061757.mongolab.com:61757/myblog"

	dial, err := mgo.Dial(MURL)

	if err != nil {
		return &SessionStore{}, err
	}
	session := dial.Clone()
	defer dial.Close()

	return &SessionStore{session, session.DB(db).C(col)}, nil
}

func (s *SessionStore) GetOne(id string) (*blog.BlogData, error) {
	var data *blog.BlogData
	err := s.Collection.Find(bson.M{"_id": id}).Sort("-created_at").One(&data)

	return data, err
}

func (s *SessionStore) GetAll() ([]blog.BlogData, error) {
	var data []blog.BlogData
	err := s.Collection.Find(bson.M{}).Sort("-created_at").All(&data)

	return data, err
}

func (s *SessionStore) Save(input *blog.BlogData) error {
	err := s.Collection.Insert(input)

	return err
}

func (s *SessionStore) Edit(id string, input map[string]interface{}) error {
	query := bson.M{"$set": input}
	err := s.Collection.Update(bson.M{"_id": id}, query)

	return err
}

func (s *SessionStore) Delete(id string) error {
	query := bson.M{"_id": id}

	err := s.Collection.Remove(query)

	return err
}
