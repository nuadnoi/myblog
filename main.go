package main

import (
	"github.com/plimble/ace"
	"github.com/plimble/ace-contrib/pongo2"
	"log"
	"myblog/blog"
	"myblog/store/mongo"
)

type Service struct {
	*blog.BlogService
}

func main() {
	sessionStore, _ := mongo.GetSession("myblog", "blog")

	//var c ace.C
	a := ace.New()
	a.Use(ace.Logger())
	tpl := &pongo2.TemplateOptions{
		Directory:     "./resources",
		IsDevelopment: true,
	}
	render := pongo2.Pongo2(tpl)
	a.HtmlTemplate(render)
	// a.Use(ace.Logger(), func(c *ace.C) {
	// if err != nil {
	// 	c.String(200, "Cannot connect to mongolab ", err)
	// 	return
	// }
	// })
	b := blog.NewService(sessionStore)

	a.GET("/", func(c *ace.C) {
		c.HTML("test.html", &name{})
		//c.String(200, "Hello World!!!")
	})

	a.GET("/blog", func(c *ace.C) {

		var result interface{}
		if id := c.MustQueryString("id", ""); id != "" {
			result = b.Get(id)
		} else {
			result = b.GetAll()
		}

		c.JSON(200, result)
	})

	a.GET("/blog/add", func(c *ace.C) {
		result, _ := b.Add()

		c.JSON(200, result)
	})

	a.GET("/blog/edit", func(c *ace.C) {
		id := c.MustQueryString("id", "")

		if id != "" {
			result, _ := b.Edit(id)
			c.JSON(200, result)

			return
		}
		c.String(200, "ID not found")

	})

	a.GET("/blog/delete", func(c *ace.C) {
		id := c.MustQueryString("id", "")

		if id != "" {
			result := b.Delete(id)
			log.Print(result)
			if result == nil {
				c.String(200, "Successful deleted")
			} else {
				c.String(200, "Nothing change")
			}
			return
		}

		c.String(200, "ID not found")
	})

	a.Run(":3001")

	//defer m.Sess.Close()

	//Search
	//err := m.BlogCol.Find(bson.M{"name": bson.RegEx{input, "i"}}).One(&r)
}
